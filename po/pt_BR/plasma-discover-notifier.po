# Translation of plasma-discover-notifier.po to Brazilian Portuguese
# Copyright (C) 2015-2020 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# André Marcelo Alvarenga <alvarenga@kde.org>, 2015, 2019, 2020.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2018, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-discover-notifier\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-14 00:47+0000\n"
"PO-Revision-Date: 2022-01-17 14:50-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.2\n"

#: notifier/DiscoverNotifier.cpp:144
#, kde-format
msgid "View Updates"
msgstr "Exibir atualizações"

#: notifier/DiscoverNotifier.cpp:261
#, kde-format
msgid "Security updates available"
msgstr "Atualizações de segurança disponíveis"

#: notifier/DiscoverNotifier.cpp:263
#, kde-format
msgid "Updates available"
msgstr "Atualizações disponíveis"

#: notifier/DiscoverNotifier.cpp:265
#, kde-format
msgid "System up to date"
msgstr "Sistema atualizado"

#: notifier/DiscoverNotifier.cpp:267
#, kde-format
msgid "Computer needs to restart"
msgstr "O computador necessita ser reiniciado"

#: notifier/DiscoverNotifier.cpp:269
#, kde-format
msgid "Offline"
msgstr "Offline"

#: notifier/DiscoverNotifier.cpp:271
#, kde-format
msgid "Applying unattended updates…"
msgstr "Aplicando atualizações autônomas..."

#: notifier/DiscoverNotifier.cpp:305
#, kde-format
msgid "Restart is required"
msgstr "A reinicialização é necessária"

#: notifier/DiscoverNotifier.cpp:306
#, kde-format
msgid "The system needs to be restarted for the updates to take effect."
msgstr "O sistema necessita ser reiniciado para as atualizações terem efeito."

#: notifier/DiscoverNotifier.cpp:312
#, kde-format
msgctxt "@action:button"
msgid "Restart"
msgstr "Reiniciar"

#: notifier/DiscoverNotifier.cpp:332
#, kde-format
msgctxt "@action:button"
msgid "Upgrade"
msgstr "Atualizar"

#: notifier/DiscoverNotifier.cpp:333
#, kde-format
msgid "Upgrade available"
msgstr "Atualização disponível"

#: notifier/DiscoverNotifier.cpp:334
#, kde-format
msgid "New version: %1"
msgstr "Nova versão: %1"

#: notifier/main.cpp:39
#, kde-format
msgid "Discover Notifier"
msgstr "Notificador do Discover"

#: notifier/main.cpp:41
#, kde-format
msgid "System update status notifier"
msgstr "Notificador do estado de atualização do sistema"

#: notifier/main.cpp:43
#, kde-format
msgid "© 2010-2022 Plasma Development Team"
msgstr "© 2010-2022 Equipe de desenvolvimento do Plasma"

#: notifier/main.cpp:47
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "André Marcelo Alvarenga"

#: notifier/main.cpp:47
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "alvarenga@kde.org"

#: notifier/main.cpp:52
#, kde-format
msgid "Replace an existing instance"
msgstr "Substituir uma instância existente"

#: notifier/main.cpp:54
#, kde-format
msgid "Do not show the notifier"
msgstr "Não mostrar o notificador"

#: notifier/main.cpp:54
#, kde-format
msgid "hidden"
msgstr "oculta"

#: notifier/NotifierItem.cpp:35 notifier/NotifierItem.cpp:36
#, kde-format
msgid "Updates"
msgstr "Atualizar"

#: notifier/NotifierItem.cpp:50
#, kde-format
msgid "Open Discover…"
msgstr "Abrir o Discover..."

#: notifier/NotifierItem.cpp:55
#, kde-format
msgid "See Updates…"
msgstr "Ver atualizações..."

#: notifier/NotifierItem.cpp:60
#, kde-format
msgid "Refresh…"
msgstr "Atualizar..."

#: notifier/NotifierItem.cpp:64
#, kde-format
msgid "Restart to apply installed updates"
msgstr "Reinicie para aplicar as atualizações instaladas"

#: notifier/NotifierItem.cpp:65
#, kde-format
msgid "Click to restart the device"
msgstr "Clique para reiniciar o dispositivo"

#~ msgid "Restart..."
#~ msgstr "Reiniciar..."
